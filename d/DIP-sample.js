class Fetch {
    request(url) {
        const dataFromFetch = 'data from fetch';
        // return fetch(url).then(res => res.json());
        return dataFromFetch;
    }
}

class LocalStorage {
    get() {
        const dataFromLocalStorage = 'data from local storage';
        // return localStorage.getItem('key');
        return dataFromLocalStorage;
    }
}

class Database {
    constructor(fetch) {
        this.fetch = fetch;
    }

    getData(key) {
        return this.fetch.request(key)
    }
}

const db1 = new Database(new Fetch());
console.log(db1.getData('random data'));
// const db2 = new Database(new LocalStorage());
// console.log(db2.getData('random data'));
// Comment casser la dépendance avec l'implémentation fetch pour utiliser le local storage ?

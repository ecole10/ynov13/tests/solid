interface IDataClient {
    get(key: string);
}

class FetchClient implements IDataClient {
    get(url: string) {
        return new Fetch().request(url);
    }
}

class LocalStorageClient implements IDataClient {
    get(key: string) {
        return new LocalStorage().get();
    }
}

class Fetch {
    request(url: string) {
        // return fetch(url).then(res => res.json());
        return 'data from fetch';
    }
}

class LocalStorage {
    get() {
        // return localStorage.getItem('key');
        return 'data from local storage';
    }
}

class Database {
    private dataClient: IDataClient;

    constructor(dataClient: IDataClient) {
        this.dataClient = dataClient;
    }

    getData(key) {
        return this.dataClient.get(key)
    }
}

const db1 = new Database(new FetchClient());
console.log(db1.getData('random data'));

const db2 = new Database(new LocalStorageClient());
console.log(db2.getData('random data'));

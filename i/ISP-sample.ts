interface IShape {
    drawCircle();
    drawSquare();
    drawRectangle();
}

class Circle implements IShape {
    drawCircle() {
    }

    drawRectangle() {
    }

    drawSquare() {
    }

}

class Square implements IShape {
    drawCircle() {
    }

    drawRectangle() {
    }

    drawSquare() {
    }
}

class Rectangle implements IShape {
    drawCircle() {
    }

    drawRectangle() {
    }

    drawSquare() {
    }
}

// ---------------- \\

interface Machine {
    print();
    fax();
    scan();
    photocopy();
}

class UserWhoOnlyScans {

}

class SimplePrinter implements Machine {
    print() {
        console.log("Print pages");
    }

    fax() {
        throw 'Can\'t fax'
    }

    scan() {
        throw 'Can\'t scan'
    }

    photocopy() {
        throw 'Can\'t photocopy'
    }
}

class MultiFunctionCopier implements Machine {
    print() {
        console.log("Print pages");
    }

    fax() {
        console.log("Fax pages");
    }

    scan() {
        console.log("Scan pages");
    }

    photocopy() {
        console.log("Photocopy pages");
    }
}

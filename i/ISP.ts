interface IShape {
    draw()
}

class Circle implements IShape {
    draw() {
        console.log('draw circle')
    }
}

class Square implements IShape {
    draw() {
        console.log('draw square')
    }
}

class Rectangle implements IShape {
    draw() {
        console.log('draw rectangle')
    }
}

// ---------------- \\

interface IPrint {
    print();
}

interface IScan {
    scan();
}

interface IFax {
    fax();
}

interface IPhotocopy {
    photocopy();
}

class UserWhoOnlyScans {
    private scanner: IScan;

    constructor(scan: IScan) {
        this.scanner = scan
    }

    useScan() {
        this.scanner.scan();
    }
}

class SimplePrinter implements IPrint {
    print() {
        console.log("Print pages");
    }
}

class MultiFunctionCopier implements IPrint, IFax, IScan, IPhotocopy {
    print() {
        console.log("Print pages");
    }

    fax() {
        console.log("Fax pages");
    }

    scan() {
        console.log("Scan pages");
    }

    photocopy() {
        console.log("Photocopy pages");
    }
}

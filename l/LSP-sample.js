function Rectangle(){
  this.height = 0;
  this.width = 0;
}

Rectangle.prototype.setHeight = function(height){
  this.height = height;
};

Rectangle.prototype.setWidth = function(width){
  this.width = width;
};

Rectangle.prototype.area = function(){
  return this.height * this.width;
};

var Square = function(){};
Square.prototype = Object.create(Rectangle.prototype);

Square.prototype.setHeight = function(height){
  this.height = height;
};

Square.prototype.setWidth = function(width){
  this.width = width;
};

var rectangle = new Rectangle();
rectangle.setWidth(4);
rectangle.setHeight(5);
console.log("Aire du rectangle : " + rectangle.area());

var square = new Square();
square.setWidth(4);
square.setHeight(5);
console.log("Aire du carré : " + square.area()); // BAD: Returns 20 for Square. Should be 25.

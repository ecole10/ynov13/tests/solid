abstract class Shape {
    abstract area(): number;
}

class Rectangle extends Shape {
    private readonly height: number;
    private readonly width: number;

    constructor(height, width) {
        super();
        this.height = height;
        this.width = width;
    }

    area(): number {
        return this.height * this.width;
    }
}

class Square extends Shape {
    private readonly size: number;

    constructor(size) {
        super();
        this.size = size;
    }

    area(): number {
        return this.size * this.size;
    }
}

let rectangle = new Rectangle(5, 4);
console.log("Aire du rectangle : " + rectangle.area());

let square = new Square(5);
console.log("Aire du carré : " + square.area());

class Product {
    constructor(name, color, size) {
        this.name = name;
        this.color = color;
        this.size = size;
    }
}

class ProductFilterColorSpecification {
    constructor(color) {
        this.color = color;
    }

    filter(products) {
        return products.filter((product) => product.color === this.color);
    }
}

class ProductFilterSizeSpecification {
    constructor(size) {
        this.size = size;
    }

    filter(products) {
        return products.filter((product) => product.size === this.size);
    }
}

class ProductFilterSizeAndColorSpecification {
    constructor(color, size) {
        this.color = color;
        this.size = size;
    }

    filter(products) {
        return products.filter(p => p.size === this.size && p.color === this.color);
    }
}

class ProductFilter {
    by(products, filterSpecification) {
        return filterSpecification.filter(products);
    }
}

let apple = new Product('Apple', 'green', 'small');
let tree = new Product('Tree', 'green', 'large');
let house = new Product('House', 'blue', 'large');
let products = [apple, tree, house];

let pf = new ProductFilter();
for (let p of pf.by(products, new ProductFilterColorSpecification('green')))
    console.log(`* ${p.name} is green`);
for (let p of pf.by(products, new ProductFilterSizeSpecification('large')))
    console.log(`- ${p.name} is large`);
for (let p of pf.by(products , new ProductFilterSizeAndColorSpecification('green', 'large')))
    console.log(`# ${p.name} is green and large`)

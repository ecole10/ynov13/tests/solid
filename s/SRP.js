class News {
    constructor(title, text) {
        this.title  = title;
        this.text = text;
        this.modified = false;
    }

    update(text)  {
        this.text = text;
        this.modified  = true;
    }

    read(format) {
        const newsParser = NewsParser.detectFormat(format)
        return newsParser.parse(this)
    }
}

class NewsParser {
    static detectFormat(format) {
        switch (format) {
            case 'html':
                return new HtmlNewsParser();
            case 'json':
                return new JsonNewsParser();
            case 'xml':
                return new XmlNewsParser();
            default:
                throw `format (${format}) not implemented`
        }
    }
}

class HtmlNewsParser {
    parse(news) {
        return `
            <div class="news">
                <h1>${news.title}</h1>
                <p>${news.text}</p>
            </div>
        `
    }
}

class JsonNewsParser {
    parse(news) {
        return JSON.stringify({
            title: news.title,
            text: news.text,
            modified: news.modified
        }, null, 2)
    }
}

class XmlNewsParser {
    parse(news) {
        return `
            <news>
                <title>${news.title}</title>
                <text>${news.text}</text>
            </news>
        `
    }
}


const news = new News('Un super titre', 'Une super description');
console.log(news.read('html'));
console.log(news.read('json'));
console.log(news.read('xml'));

news.update('Une description encore mieux !');
console.log(news.read('html'));
console.log(news.read('json'));
console.log(news.read('xml'));
